package deloitte.academy.lesson01.entity;

public enum CategoriaCliente {

	ClienteNormal, ViajeroFrecuente, Empleado;
}
