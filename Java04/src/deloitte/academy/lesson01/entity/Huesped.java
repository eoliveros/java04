package deloitte.academy.lesson01.entity;

import java.util.ArrayList;

public abstract class Huesped {

	private CategoriaCliente a;
	private String nombre;
	private int numeroHabitacion;
	private double importe;

	public Huesped(CategoriaCliente clienteNormal, String nombre, int numeroHabitacion, double importe) {
		super();
		a = clienteNormal;
		this.nombre = nombre;
		this.numeroHabitacion = numeroHabitacion;
		this.importe = importe;
	}

	public CategoriaCliente getClienteNormal() {
		return a;
	}

	public void setClienteNormal(CategoriaCliente clienteNormal) {
		a = clienteNormal;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumeroHabitacion() {
		return numeroHabitacion;
	}

	public void setNumeroHabitacion(int numeroHabitacion) {
		this.numeroHabitacion = numeroHabitacion;
	}

	public double getImporte() {
		return importe;
	}

	public void setImporte(double importe) {
		this.importe = importe;
	}

	public abstract double cobrar();

}
