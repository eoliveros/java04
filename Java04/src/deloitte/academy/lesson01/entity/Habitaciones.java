package deloitte.academy.lesson01.entity;

public class Habitaciones {

	private int numeroHabitacion;
	private boolean disponibilidadHabitacion;

	public Habitaciones(int numeroHabitacion, boolean disponibilidadHabitacion) {
		super();
		this.numeroHabitacion = numeroHabitacion;
		this.disponibilidadHabitacion = disponibilidadHabitacion;
	}

	public int getNumeroHabitacion() {
		return numeroHabitacion;
	}

	public void setNumeroHabitacion(int numeroHabitacion) {
		this.numeroHabitacion = numeroHabitacion;
	}

	public boolean isDisponibilidadHabitacion() {
		return disponibilidadHabitacion;
	}

	public void setDisponibilidadHabitacion(boolean disponibilidadHabitacion) {
		this.disponibilidadHabitacion = disponibilidadHabitacion;
	}

}
