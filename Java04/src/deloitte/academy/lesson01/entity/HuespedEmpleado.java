package deloitte.academy.lesson01.entity;

import java.util.logging.Logger;

public class HuespedEmpleado extends Huesped {
	private static final Logger LOGGER = Logger.getLogger(HuespedEmpleado.class.getName());

	public HuespedEmpleado(CategoriaCliente clienteNormal, String nombre, int numeroHabitacion, double importe) {
		super(clienteNormal, nombre, numeroHabitacion, importe);
		// TODO Auto-generated constructor stub
	}

	@Override
	public double cobrar() {
		// TODO Auto-generated method stub
		LOGGER.info("Importe con descuento");
		return this.getImporte() * .5;

	}

}