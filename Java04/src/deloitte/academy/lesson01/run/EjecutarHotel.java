package deloitte.academy.lesson01.run;
import deloitte.academy.lesson01.entity.CategoriaCliente;
import deloitte.academy.lesson01.entity.Habitaciones;
import deloitte.academy.lesson01.entity.Huesped;
import deloitte.academy.lesson01.entity.HuespedClienteNormal;
import deloitte.academy.lesson01.entity.HuespedEmpleado;
import deloitte.academy.lesson01.entity.HuespedViajeroFrecuente;
import deloitte.academy.lesson01.logic.Hotel;

import java.util.ArrayList;
import java.util.List;


public class EjecutarHotel {

	public static List<Huesped> ListaHuespedes = new ArrayList<Huesped>();
	public static List<Habitaciones> ListaHabitaciones = new ArrayList<Habitaciones>();
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		HuespedClienteNormal a1 = new HuespedClienteNormal(CategoriaCliente.ClienteNormal, "Rodrigo", 4, 2000);
		HuespedViajeroFrecuente a2 = new HuespedViajeroFrecuente(CategoriaCliente.ViajeroFrecuente, "Esteban", 4, 2000);
		HuespedViajeroFrecuente a3 = new HuespedViajeroFrecuente(CategoriaCliente.ViajeroFrecuente, "Fatima", 4, 2000);
		HuespedEmpleado a4 = new HuespedEmpleado(CategoriaCliente.Empleado, "Leticia", 4, 2000);
		
		Hotel hotel = new Hotel();
		String resultado = Hotel.CheckIn(a1);
		System.out.println(resultado);
		
		
		
	}

}
